import Styles, {
	scale,
	verticalScale,
	moderateScale,
	getPaddingDirection,
	getMarginDirection,
} from './Styles';

export {
	Styles as default,
	scale,
	verticalScale,
	moderateScale,
	getPaddingDirection,
	getMarginDirection,
};
