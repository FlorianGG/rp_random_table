import { StyleSheet } from 'react-native';

import CONSTANTES from '../services';

const { width, height } = CONSTANTES.dimension;

// inspired form article https://medium.com/soluto-engineering/size-matters-5aeeb462900a
// Guideline sizes are based on standard ~5" screen mobile device

const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => (width / guidelineBaseWidth) * size;
const verticalScale = size => (height / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor;

const getPaddingDirection = (value, direction) => {
	switch (direction) {
		case 'top':
			return {
				paddingTop: verticalScale(value),
			};
		case 'bottom':
			return {
				paddingBottom: verticalScale(value),
			};
		case 'vertical':
			return {
				paddingVertical: verticalScale(value),
			};
		case 'left':
			return {
				paddingLeft: scale(value),
			};
		case 'right':
			return {
				paddingRight: scale(value),
			};
		case 'horizontal':
			return {
				paddingHorizontal: scale(value),
			};
		default:
			return {
				paddingHorizontal: scale(value),
				paddingVertical: verticalScale(value),
			};
	}
};

const getMarginDirection = (value, direction) => {
	switch (direction) {
		case 'top':
			return {
				marginTop: verticalScale(value),
			};
		case 'bottom':
			return {
				marginBottom: verticalScale(value),
			};
		case 'vertical':
			return {
				marginVertical: verticalScale(value),
			};
		case 'left':
			return {
				marginLeft: scale(value),
			};
		case 'right':
			return {
				marginRight: scale(value),
			};
		case 'horizontal':
			return {
				marginHorizontal: scale(value),
			};
		default:
			return {
				marginHorizontal: scale(value),
				marginVertical: verticalScale(value),
			};
	}
};

const styles = StyleSheet.create({
	flex: {
		flex: 1,
	},
	centeredContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	verticallyCenteredContainer: {
		flex: 1,
		justifyContent: 'center',
	},
});

export {
	styles as default,
	scale,
	verticalScale,
	moderateScale,
	getPaddingDirection,
	getMarginDirection,
};
