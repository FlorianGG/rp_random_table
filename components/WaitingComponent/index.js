import PropTypes from 'prop-types';
import React from 'react';
import { Layout, Text, Spinner, Card, CardHeader, Icon } from '@ui-kitten/components';

import Styles from '../../styles';

const Header = () => <CardHeader title="Une erreur est survenue" />;
const CrossIson = style => <Icon {...style} name="close-outline" />;

const WaitingComponent = ({ isLoading, error }) => {
	return error ? (
		<Layout style={Styles.verticallyCenteredContainer} level="3">
			<Card status="danger">
				<Text>{error}</Text>
			</Card>
		</Layout>
	) : isLoading ? (
		<Layout level="3" style={Styles.centeredContainer}>
			<Spinner status="warning" size="giant" />
		</Layout>
	) : null;
};

export default WaitingComponent;

WaitingComponent.propTypes = {
	isLoading: PropTypes.bool,
	error: PropTypes.string,
};
WaitingComponent.defaultProps = {
	isLoading: true,
	error: null,
};
