import PropTypes from 'prop-types';
import React, { useContext, memo } from 'react';

import { ListItem, Icon, Layout, Text } from '@ui-kitten/components';

import ParametersContext from '../../hooks/context/Parameters';

import styles from './styles';

const icon = style => <Icon name="cube" {...style} />;

const renderAccessory = rollDice => {
	const test = rollDice.match(/^([0-9]{1,2})_([0-9]{1,3})$/);
	if (test) {
		return (
			<Layout style={styles.wrapperCircle}>
				<Layout level="4" style={styles.circle}>
					{test[1] ? <Text>{test[1]}</Text> : <Text>?</Text>}
				</Layout>
				<Text> - </Text>
				<Layout level="4" style={styles.circle}>
					{test[2] ? <Text>{test[2]}</Text> : <Text>?</Text>}
				</Layout>
			</Layout>
		);
	} else {
		return (
			<Layout level="4" style={styles.circle}>
				<Text>{rollDice}</Text>
			</Layout>
		);
	}
};

export const ListItemTableDice = ({ item, index, theme }) => {
	const parametersContext = useContext(ParametersContext);
	const text =
		!parametersContext || (parametersContext && parametersContext.parameters.isFrenchLanguage)
			? item.french_text
			: item.english_text;

	const backgrounColorStyle =
		index % 2 === 0
			? { backgroundColor: theme['background-basic-color-2'] }
			: { backgroundColor: theme['background-basic-color-3'] };
	return (
		<ListItem
			title={text}
			icon={icon}
			accessory={() => renderAccessory(item.roll_dice)}
			style={backgrounColorStyle}
		/>
	);
};

export default memo(ListItemTableDice);

ListItemTableDice.propTypes = {
	item: PropTypes.shape({
		french_text: PropTypes.string.isRequired,
		english_text: PropTypes.string.isRequired,
		roll_dice: PropTypes.string.isRequired,
	}).isRequired,
	theme: PropTypes.shape({
		'background-basic-color-2': PropTypes.string.isRequired,
		'background-basic-color-3': PropTypes.string.isRequired,
	}).isRequired,
	index: PropTypes.number.isRequired,
};
ListItemTableDice.defaultProps = {};
