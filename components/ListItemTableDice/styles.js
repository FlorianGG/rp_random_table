import { StyleSheet } from 'react-native';

import { scale } from '../../styles';

const styles = StyleSheet.create({
	wrapperCircle: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'transparent',
	},
	circle: {
		width: scale(30),
		height: scale(30),
		borderRadius: scale(30 / 2),
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export { styles as default };
