import PropTypes from 'prop-types';
import React, { useContext, memo } from 'react';

import { ListItem as ListItemKitten } from '@ui-kitten/components';

import ParametersContext from '../../hooks/context/Parameters';

export const ListItem = ({ item, icon, onPress }) => {
	const parametersContext = useContext(ParametersContext);
	const text =
		!parametersContext || (parametersContext && parametersContext.parameters.isFrenchLanguage)
			? item.french_text
			: item.english_text;
	return <ListItemKitten title={text} icon={icon} onPress={onPress} />;
};
export default memo(ListItem);

ListItem.propTypes = {
	item: PropTypes.shape({
		french_text: PropTypes.string.isRequired,
		english_text: PropTypes.string.isRequired,
	}).isRequired,
	icon: PropTypes.func,
	onPress: PropTypes.func,
};
ListItem.defaultProps = {
	icon: null,
	onPress: null,
};
