import { StyleService } from '@ui-kitten/components';
import { getPaddingDirection } from '../../styles';

const styles = StyleService.create({
	containerCenter: {
		backgroundColor: 'color-info-300',
		...getPaddingDirection(10),
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
});

export { styles as default };
