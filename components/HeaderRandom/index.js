import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import { Text, Layout, Spinner, useStyleSheet } from '@ui-kitten/components';

import ParametersContext from '../../hooks/context/Parameters';

import styles from './styles';

const HeaderRandom = ({ item, table }) => {
	const themedStyles = useStyleSheet(styles);
	const parametersContext = useContext(ParametersContext);
	const text =
		!parametersContext || (parametersContext && parametersContext.parameters.isFrenchLanguage)
			? table.french_text
			: table.english_text;
	return item ? (
		<Layout style={themedStyles.containerCenter}>
			<Text>{text}</Text>
		</Layout>
	) : (
		<Layout style={themedStyles.containerCenter}>
			<Spinner status="warning" size="small" />
		</Layout>
	);
};

export default HeaderRandom;

HeaderRandom.propTypes = {
	item: PropTypes.shape({
		french_text: PropTypes.string.isRequired,
		english_text: PropTypes.string.isRequired,
		roll_dice: PropTypes.string.isRequired,
	}),
	table: PropTypes.shape({
		french_text: PropTypes.string.isRequired,
		english_text: PropTypes.string.isRequired,
	}).isRequired,
};
HeaderRandom.defaultProps = {
	item: null,
};
