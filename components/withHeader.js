import React, { useCallback } from 'react';

import { Divider, Icon, TopNavigation, TopNavigationAction } from '@ui-kitten/components';

const BackIcon = style => <Icon {...style} name="arrow-back" />;

const withNavigationTopBar = Component => props => {
	const { route, navigation, parameters, translate } = props;
	const title =
		translate && parameters ? translate(route.name.toLowerCase(), parameters.isFrenchLanguage) : '';

	const navigateBack = useCallback(() => {
		navigation.goBack();
	}, [navigation]);

	const backAction = useCallback(
		() =>
			route.name !== 'Home' ? <TopNavigationAction icon={BackIcon} onPress={navigateBack} /> : null,
		[route]
	);

	return (
		<>
			<TopNavigation
				title={title}
				alignment="center"
				// rightControls={renderRightControls()}
				leftControl={backAction()}
			/>
			<Divider />
			<Component {...props} />
		</>
	);
};

export { withNavigationTopBar as default };
