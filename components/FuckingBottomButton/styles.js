import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	button: {
		position: 'absolute',
		bottom: 10,
		right: 10,
		width: 50,
		height: 50,
		borderRadius: 50 / 2,
		alignItems: 'center',
		justifyContent: 'center',
	},
});

export { styles as default };
