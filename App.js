import React, { useState, useCallback, useEffect } from 'react';
import { SafeAreaView, YellowBox, View, ActivityIndicator } from 'react-native';

import * as RNLocalize from 'react-native-localize';
import AsyncStorage from '@react-native-community/async-storage';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { mapping, light, dark } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

import appTheme from './styles/custom-theme.json'; // <-- Import app theme
import customMapping from './styles/custom-mapping.json';

import { withDatabaseProvider } from './hooks/context/Database';

import ParametersContext from './hooks/context/Parameters';

import AppNavigator from './services/navigation';

import Styles from './styles';

YellowBox.ignoreWarnings(['Remote debugger']);
const themes = { light: { ...light, ...appTheme.light }, dark: { ...dark, ...appTheme.dark } };

const App = () => {
	const [parameters, setParameters] = useState(null);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		console.log('salut');
		const initLanguageState = AsyncStorage.getItem('languagePersistantState')
			.then(res => res)
			.catch(err => {
				console.log('-------err-------');
				console.log(err);
				return null;
			});
		const initThemeState = AsyncStorage.getItem('themePersistantState')
			.then(res => res)
			.catch(err => {
				console.log('-------err-------');
				console.log(err);
				return 'light';
			});
		Promise.all([initLanguageState, initThemeState]).then(resPromises => {
			const [isFrenchLanguageStore, themeStore] = resPromises;
			const newParameters = {
				theme: 'dark',
				isFrenchLanguage: RNLocalize.getCountry() === 'FR',
			};
			if (isFrenchLanguageStore != null) {
				newParameters.isFrenchLanguage = JSON.parse(isFrenchLanguageStore);
			}
			if (themeStore === 'light' || themeStore === 'dark') {
				newParameters.theme = themeStore;
			}
			setParameters(newParameters);
		});
	}, []);

	useEffect(() => {
		if (isLoading && parameters !== null) {
			setIsLoading(false);
		}
	}, [parameters]);

	const currentTheme = parameters !== null && themes[parameters.theme];

	const toggleTheme = useCallback(() => {
		const newTheme = parameters.theme === 'light' ? 'dark' : 'light';
		return AsyncStorage.setItem('themePersistantState', newTheme).then(() =>
			setParameters(
				parameters !== null && {
					...parameters,
					theme: newTheme,
				}
			)
		);
	}, [parameters]);
	const toggleLanguage = useCallback(() => {
		const newLanguage = !parameters.isFrenchLanguage;
		return AsyncStorage.setItem('languagePersistantState', JSON.stringify(newLanguage)).then(() =>
			setParameters(parameters !== null && { ...parameters, isFrenchLanguage: newLanguage })
		);
	}, [parameters]);

	const bgColor = currentTheme
		? currentTheme[currentTheme['background-basic-color-1'].replace('$', '')]
		: '#00174B';
	return (
		<>
			<IconRegistry icons={EvaIconsPack} />
			<ParametersContext.Provider value={{ parameters, toggleTheme, toggleLanguage }}>
				<ApplicationProvider mapping={mapping} theme={currentTheme} customMapping={customMapping}>
					<>
						<SafeAreaView
							style={{
								flex: 0,
								backgroundColor: bgColor,
							}}
						/>
						<SafeAreaView
							style={{
								flex: 1,
								backgroundColor: bgColor,
							}}
						>
							{isLoading ? (
								<View style={Styles.centeredContainer}>
									<ActivityIndicator size="large" color="#0000ff" />
								</View>
							) : (
								<AppNavigator />
							)}
						</SafeAreaView>
					</>
				</ApplicationProvider>
			</ParametersContext.Provider>
		</>
	);
};

export default withDatabaseProvider(App);
