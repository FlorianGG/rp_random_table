import CONSTANTES from './constante';

const { translation } = CONSTANTES;
/**
 * Modify the case of the first letter of the string to uppercase
 * @param {string} string
 */
function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param {string} key
 * @param {boolean} isFrenchLanguage
 * @param {*} firstLetterUpperCase
 */
function returnText(key, isFrenchLanguage, firstLetterUpperCase = true) {
	if (typeof key === 'string' && key.length) {
		const textObj = translation.get(key);
		if (textObj) {
			let text;
			if (isFrenchLanguage) {
				text = textObj.french_text || '';
				return firstLetterUpperCase ? capitalizeFirstLetter(text) : text;
			} else {
				text = textObj.english_text || '';
				return firstLetterUpperCase ? capitalizeFirstLetter(text) : text;
			}
		}
	}
	return '';
}

export { capitalizeFirstLetter, returnText };
