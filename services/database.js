import SQLite from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-community/async-storage';
import datas from './datas.json';

export default class Database {
	static instance;

	constructor() {
		if (!Database.instance) {
			Database.instance = this;
			this.init = false;
			this.allMeetingTable = null;
			this.allTreasureTable = null;
			this.allLinesByTable = new Map();
		}
		return Database.instance;
	}

	/**
	 * Function in charge of the initialization of the Database
	 * It check if there are a datas.json files
	 * If yes we create a new database
	 */
	initDatabase() {
		const { version } = datas;
		return AsyncStorage.getItem('version')
			.then(async res => {
				if (res === null || Number.parseFloat(res) !== version) {
					try {
						await this.deleteDatabase();
						await this.openDatabase();
						await this.truncateAllTable();

						await this.insertCategories();
						await this.insertTags();
						await this.insertPosts();
						await this.insertPostCategoryLinks();
						await this.insertPostTagLinks();
						await this.insertPostLines();

						await AsyncStorage.setItem('version', version.toString());

						this.init = true;
						return this;
					} catch (error) {
						throw new Error(error);
					}
				} else {
					try {
						await this.openDatabase();
						this.init = true;
						return this;
					} catch (error) {
						this.init = false;
						console.log('-------error-------');
						console.log(error);
						throw new Error(error.message);
					}
				}
			})
			.catch(err => {
				throw new Error(err.message);
			});
	}

	/**
	 * Function which delete the database
	 */
	deleteDatabase() {
		return new Promise(resolve => {
			SQLite.deleteDatabase(
				{ name: `random_table_db.sqlite3`, location: 1 },
				() => resolve(true),
				() => resolve(true)
			);
		});
	}

	/**
	 * Function which open the database
	 */
	openDatabase() {
		return new Promise((resolve, reject) => {
			this.db = SQLite.openDatabase(
				{
					name: `random_table_db.sqlite3`,
					createFromLocation: 1,
				},
				() => {
					resolve(true);
				},
				err => {
					reject(err.message);
				}
			);
		});
	}

	/**
	 * Function which truncates all tables
	 */
	truncateAllTable() {
		return Promise.all([
			this.truncateTable('categories'),
			this.truncateTable('tags'),
			this.truncateTable('post_category_links'),
			this.truncateTable('post_lines'),
			this.truncateTable('post_tag_links'),
			this.truncateTable('posts'),
		])
			.then(() => true)
			.catch(err => {
				throw new Error(err);
			});
	}

	/**
	 * Genric function to truncate a table
	 * @param {string} tablename
	 */
	truncateTable(tablename) {
		return new Promise((resolve, reject) => {
			this.db.transaction(
				tx => {
					tx.executeSql(
						`DELETE FROM ${tablename};`,
						[],
						() => {
							resolve(true);
						},
						err => {
							reject(err.message);
						}
					);
				},
				err => {
					reject(err.message);
				}
			);
		});
	}

	/**
	 * Generic function which execute the sql
	 * @param {string} sql
	 */
	fetchSelect(sql) {
		return new Promise((resolve, reject) => {
			this.db.transaction(
				txn => {
					txn.executeSql(
						sql,
						[],
						(tx, res) => {
							resolve(res.rows.raw());
						},
						err => {
							reject(err.message);
						}
					);
				},
				err => {
					reject(err.message);
				}
			);
		});
	}

	/**
	 * Function which insert categories
	 */
	insertCategories() {
		const { categories } = datas;
		let sql = 'INSERT INTO categories (id, french_text, english_text) VALUES';
		for (let i = 0; i < categories.length; i++) {
			if (i === categories.length - 1) {
				sql += ` (${categories[i].id}, '${categories[i].french_text}', '${categories[i].english_text}');`;
			} else {
				sql += ` (${categories[i].id}, '${categories[i].french_text}', '${categories[i].english_text}'),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which insert tags
	 */
	insertTags() {
		const { tags } = datas;
		let sql = 'INSERT INTO tags (id, french_text, english_text) VALUES';
		for (let i = 0; i < tags.length; i++) {
			if (i === tags.length - 1) {
				sql += ` (${tags[i].id}, '${tags[i].french_text}', '${tags[i].english_text}');`;
			} else {
				sql += ` (${tags[i].id}, '${tags[i].french_text}', '${tags[i].english_text}'),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which insert posts
	 */
	insertPosts() {
		const { posts } = datas;
		let sql = 'INSERT INTO posts (id, french_text, english_text) VALUES';
		for (let i = 0; i < posts.length; i++) {
			if (i === posts.length - 1) {
				sql += ` (${posts[i].id}, '${posts[i].french_text}', '${posts[i].english_text}');`;
			} else {
				sql += ` (${posts[i].id}, '${posts[i].french_text}', '${posts[i].english_text}'),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which insert post_category_links
	 */
	insertPostCategoryLinks() {
		const postCategoryLinks = datas.post_category;
		let sql = 'INSERT INTO post_category_links (post_id, category_id) VALUES';
		for (let i = 0; i < postCategoryLinks.length; i++) {
			if (i === postCategoryLinks.length - 1) {
				sql += ` (${postCategoryLinks[i].post_id}, ${postCategoryLinks[i].category_id});`;
			} else {
				sql += ` (${postCategoryLinks[i].post_id}, ${postCategoryLinks[i].category_id}),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which insert post_tag_links
	 */
	insertPostTagLinks() {
		const postTagLinks = datas.post_tag;
		let sql = 'INSERT INTO post_tag_links (post_id, tag_id) VALUES';
		for (let i = 0; i < postTagLinks.length; i++) {
			if (i === postTagLinks.length - 1) {
				sql += ` (${postTagLinks[i].post_id}, ${postTagLinks[i].tag_id});`;
			} else {
				sql += ` (${postTagLinks[i].post_id}, ${postTagLinks[i].tag_id}),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which insert post_lines
	 */
	insertPostLines() {
		const postLines = datas.post_line;
		let sql = 'INSERT INTO post_lines (post_id, roll_dice, french_text, english_text) VALUES';
		for (let i = 0; i < postLines.length; i++) {
			if (i === postLines.length - 1) {
				sql += ` (${postLines[i].post_id}, '${postLines[i].roll_dice}', '${postLines[i].french_text}', '${postLines[i].english_text}');`;
			} else {
				sql += ` (${postLines[i].post_id}, '${postLines[i].roll_dice}', '${postLines[i].french_text}', '${postLines[i].english_text}'),`;
			}
		}
		return this.fetchSelect(sql);
	}

	/**
	 * Function which get back all tables for meeting category
	 */
	selectAllMeetingTable() {
		if (this.allMeetingTable) {
			return new Promise(resolve => resolve(this.allMeetingTable));
		} else {
			const sql =
				'SELECT * FROM posts INNER JOIN post_category_links ON posts.id = post_category_links.post_id AND post_category_links.category_id = 24';
			return this.fetchSelect(sql).then(res => {
				this.allMeetingTable = res;
				return this.allMeetingTable;
			});
		}
	}

	/**
	 * Function which get back all tables for treasure category
	 */
	selectAllTreasureTable() {
		if (this.allTreasureTable) {
			return new Promise(resolve => resolve(this.allTreasureTable));
		} else {
			const sql =
				'SELECT * FROM posts INNER JOIN post_category_links ON posts.id = post_category_links.post_id AND post_category_links.category_id = 8';
			return this.fetchSelect(sql).then(res => {
				this.allTreasureTable = res;
				return this.allTreasureTable;
			});
		}
	}

	/**
	 * Function which get back all lines from one table
	 * @param {integer} tableId
	 */
	selectAllLinesFormOneTable(tableId) {
		const lines = this.allLinesByTable.get(tableId);
		if (lines) {
			return new Promise(resolve => resolve(lines));
		} else {
			const sql = `SELECT * FROM post_lines WHERE post_lines.post_id = ${tableId}`;
			return this.fetchSelect(sql).then(res => {
				this.allLinesByTable.set(tableId, res);
				return res;
			});
		}
	}
}
