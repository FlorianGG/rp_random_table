import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
const CONSTANTES = {
	dimension: {
		height,
		width,
	},
	translation: new Map([
		['home', { english_text: 'home', french_text: 'accueil' }],
		[
			'tablerandommeetinglist',
			{ english_text: 'meetings tables', french_text: 'tables des rencontres' },
		],
		[
			'tablerandomtreasurelist',
			{ english_text: 'treasures tables', french_text: 'tables des trésors' },
		],
		[
			'tablerandomtreasure',
			{ english_text: 'random treasures table', french_text: 'table des trésors aléatoire' },
		],
		[
			'tablerandommeeting',
			{ english_text: 'random meetings table', french_text: 'table des rencontres aléatoire' },
		],
		['parameters', { english_text: 'parameters', french_text: 'paramètres' }],
		['language', { english_text: 'language', french_text: 'langue' }],
		['english', { english_text: 'english', french_text: 'anglais' }],
		['french', { english_text: 'french', french_text: 'français' }],
		['light', { english_text: 'light', french_text: 'clair' }],
		['dark', { english_text: 'dark', french_text: 'sombre' }],
		['appearance', { english_text: 'appearance', french_text: 'apparence' }],
		[
			'update_parameters_with_switch',
			{
				english_text: 'update parameters with handle toggle field',
				french_text: 'modifier les paramètres avec les champs switch',
			},
		],
		['meetings_tables', { english_text: 'meetings tables', french_text: 'tables des rencontres' }],
		['treasures_tables', { english_text: 'treasures tables', french_text: 'tables des trésors' }],
		['about', { english_text: 'about', french_text: 'à propos' }],
		[
			'nothing_to_display',
			{
				english_text: 'ouch!! There is nothing to display!',
				french_text: `oups!! Il n'y a rien à afficher!`,
			},
		],
		[
			'close',
			{
				english_text: 'close',
				french_text: `fermer`,
			},
		],
	]),
};

export { CONSTANTES as default };
