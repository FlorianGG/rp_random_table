import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { compose } from 'recompose';

import AboutScreen from '../screens/AboutScreen';
import ErrorScreen from '../screens/ErrorScreen';
import HomeScreen from '../screens/HomeScreen';
import ParametersScreen from '../screens/ParametersScreen';
import TableListMeetingScreen from '../screens/TableListMeetingScreen';
import TableListTreasureScreen from '../screens/TableListTreasureScreen';
import TableRandomScreen from '../screens/TableRandomScreen';

import withNavigationTopBar from '../components/withHeader';
import { withDatabaseConsumer } from '../hooks/context/Database';
import { withParametersContextConsumer } from '../hooks/context/Parameters';

const Stack = createStackNavigator();

const TableListMeetingStackNavigator = () => (
	<Stack.Navigator headerMode="none" initialRouteName="TableList">
		<Stack.Screen
			name="TableRandomMeetingList"
			component={compose(
				withDatabaseConsumer,
				withParametersContextConsumer,
				withNavigationTopBar
			)(TableListMeetingScreen)}
		/>
		<Stack.Screen
			name="TableRandomMeeting"
			component={compose(
				withDatabaseConsumer,
				withParametersContextConsumer,
				withNavigationTopBar
			)(TableRandomScreen)}
		/>
	</Stack.Navigator>
);
const TableListTreasureStackNavigator = () => (
	<Stack.Navigator headerMode="none" initialRouteName="TableList">
		<Stack.Screen
			name="TableRandomTreasureList"
			component={compose(
				withDatabaseConsumer,
				withParametersContextConsumer,
				withNavigationTopBar
			)(TableListTreasureScreen)}
		/>
		<Stack.Screen
			name="TableRandomTreasure"
			component={compose(
				withDatabaseConsumer,
				withParametersContextConsumer,
				withNavigationTopBar
			)(TableRandomScreen)}
		/>
	</Stack.Navigator>
);
const MainNavigator = () => (
	<Stack.Navigator headerMode="none" initialRouteName="Home">
		<Stack.Screen
			name="Home"
			component={compose(
				withDatabaseConsumer,
				withParametersContextConsumer,
				withNavigationTopBar
			)(HomeScreen)}
		/>
		<Stack.Screen name="TableListMeetingStack" component={TableListMeetingStackNavigator} />
		<Stack.Screen name="TableListTreasureStack" component={TableListTreasureStackNavigator} />
		<Stack.Screen
			name="About"
			component={compose(withParametersContextConsumer, withNavigationTopBar)(AboutScreen)}
		/>
		<Stack.Screen
			name="Parameters"
			component={compose(withParametersContextConsumer, withNavigationTopBar)(ParametersScreen)}
		/>
		<Stack.Screen
			name="Error"
			component={compose(withDatabaseConsumer, withParametersContextConsumer)(ErrorScreen)}
			title="test"
		/>
	</Stack.Navigator>
);

const AppNavigator = () => (
	<NavigationContainer>
		<MainNavigator />
	</NavigationContainer>
);

export { AppNavigator as default };
