import PropTypes from 'prop-types';

const parametersProptypes = {
	parameters: PropTypes.shape({
		theme: PropTypes.oneOf(['light', 'dark']).isRequired,
		isFrenchLanguage: PropTypes.bool.isRequired,
	}).isRequired,
	toggleLanguage: PropTypes.func.isRequired,
	toggleTheme: PropTypes.func.isRequired,
	translate: PropTypes.func.isRequired,
};

const navigationProptypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

const databaseProptypes = {
	database: PropTypes.oneOfType([PropTypes.object, PropTypes.oneOf([false])]).isRequired,
};

const fullProptypes = {
	...parametersProptypes,
	...navigationProptypes,
	...databaseProptypes,
};
export { parametersProptypes, navigationProptypes, databaseProptypes, fullProptypes };
