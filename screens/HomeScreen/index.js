import React, { useCallback, useEffect } from 'react';
import { Layout, Button, Icon } from '@ui-kitten/components';

import { PROPTYPES } from '../../services';

import styles from './styles';

const ListIcon = style => <Icon name="list-outline" {...style} />;

const AboutIcon = style => <Icon name="alert-circle-outline" {...style} />;
const CorgIcon = style => <Icon name="settings-2-outline" {...style} />;
const HomeScreen = ({ navigation, database, parameters, translate }) => {
	useEffect(() => {
		if (database.init === false || database === false) {
			navigation.navigate('Error');
		}
	}, [database]);

	const navigateToTableListMeeting = useCallback(() => {
		navigation.navigate('TableListMeetingStack');
	}, [navigation]);
	const navigateToTableListTreasure = useCallback(() => {
		navigation.navigate('TableListTreasureStack');
	}, [navigation]);
	const navigateToParameters = useCallback(() => {
		navigation.navigate('Parameters');
	}, [navigation]);
	const navigateToAbout = useCallback(() => {
		navigation.navigate('About');
	}, [navigation]);
	return (
		<Layout style={styles.container} level="3">
			<Button
				icon={ListIcon}
				style={styles.button}
				onPress={navigateToTableListMeeting}
				status="info"
			>
				{translate('meetings_tables', parameters.isFrenchLanguage)}
			</Button>
			<Button
				icon={ListIcon}
				style={styles.button}
				onPress={navigateToTableListTreasure}
				status="info"
			>
				{translate('treasures_tables', parameters.isFrenchLanguage)}
			</Button>
			<Button icon={CorgIcon} style={styles.button} onPress={navigateToParameters} status="success">
				{translate('parameters', parameters.isFrenchLanguage)}
			</Button>
			<Button icon={AboutIcon} style={styles.button} onPress={navigateToAbout} status="danger">
				{translate('about', parameters.isFrenchLanguage)}
			</Button>
		</Layout>
	);
};

HomeScreen.propTypes = {
	...PROPTYPES.fullProptypes,
};

HomeScreen.defaultProps = {};

export { HomeScreen as default };
