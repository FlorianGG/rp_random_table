import { StyleSheet } from 'react-native';

import { getPaddingDirection, getMarginDirection } from '../../styles';

const styles = StyleSheet.create({
	container: {
		...getPaddingDirection(10),
		flex: 1,
		justifyContent: 'center',
	},
	button: {
		...getMarginDirection(10, 'vertical'),
	},
});

export { styles as default };
