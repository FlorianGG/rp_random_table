import { StyleSheet } from 'react-native';

import { getPaddingDirection, getMarginDirection } from '../../styles';

const styles = StyleSheet.create({
	container: {
		...getPaddingDirection(10),
		flex: 1,
	},
	card: {
		...getMarginDirection(5, 'vertical'),
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		textAlignVertical: 'center',
		...getMarginDirection(5, 'vertical'),
	},
	textLabel: {
		flex: 1,
		fontSize: 16,
		fontWeight: 'bold',
		textAlign: 'left',
	},
	switch: {
		flex: 1,
		justifyContent: 'flex-start',
	},
});

export { styles as default };
