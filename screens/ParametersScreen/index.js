import React, { useCallback } from 'react';

import { Layout, Text, Toggle, CardHeader, Card } from '@ui-kitten/components';

import { PROPTYPES } from '../../services';

import styles from './styles';

const ParametersScreen = ({ parameters, toggleLanguage, toggleTheme, translate }) => {
	const Header = useCallback(
		() => (
			<CardHeader
				title={translate('parameters', parameters.isFrenchLanguage)}
				description={translate('update_parameters_with_switch', parameters.isFrenchLanguage)}
			/>
		),
		[parameters]
	);
	return (
		<Layout style={styles.container} level="3">
			<Card header={Header} status="warning" style={styles.card} level="2">
				<Layout style={styles.row}>
					<Text style={styles.textLabel}>
						{`${translate('language', parameters.isFrenchLanguage)} :`}
					</Text>
					<Toggle
						checked={parameters.isFrenchLanguage}
						text={
							parameters.isFrenchLanguage
								? translate('french', parameters.isFrenchLanguage)
								: translate('english', parameters.isFrenchLanguage)
						}
						textStyle={styles.text}
						onChange={toggleLanguage}
						status="warning"
						style={styles.switch}
					/>
				</Layout>
				<Layout style={styles.row}>
					<Text style={styles.textLabel}>{`${translate(
						'appearance',
						parameters.isFrenchLanguage
					)} :`}</Text>
					<Toggle
						checked={parameters.theme === 'dark'}
						text={
							parameters.theme === 'dark'
								? translate('dark', parameters.isFrenchLanguage)
								: translate('light', parameters.isFrenchLanguage)
						}
						textStyle={styles.text}
						onChange={toggleTheme}
						status="warning"
						style={styles.switch}
					/>
				</Layout>
			</Card>
		</Layout>
	);
};

export default ParametersScreen;

ParametersScreen.propTypes = {
	...PROPTYPES.parametersProptypes,
};
ParametersScreen.defaultProps = {};
