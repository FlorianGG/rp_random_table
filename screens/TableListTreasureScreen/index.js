import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { Icon, List } from '@ui-kitten/components';

import WaitingComponent from '../../components/WaitingComponent';
import ListItem from '../../components/ListItem';

const StarIcon = style => <Icon name="star" {...style} />;

const TableListTreasureScreen = ({ navigation, database }) => {
	const [isLoading, setIsLoading] = useState(true);
	const [error, setError] = useState(null);
	const [data, setData] = useState([]);
	useEffect(() => {
		database
			.selectAllTreasureTable()
			.then(res => {
				setData(res);
			})
			.catch(err => {
				console.log('-------err-------');
				console.log(err);
			});
	}, []);

	useEffect(() => {
		if (data.length) {
			setIsLoading(false);
		}
	}, [data]);

	const ListItemWithIcon = useCallback(params => {
		const navigate = () => navigation.navigate('TableRandomTreasure', { table: params.item });
		return <ListItem {...params} icon={StarIcon} onPress={navigate} />;
	}, []);

	return isLoading ? (
		<WaitingComponent isLoading={isLoading} error={error} />
	) : (
		<List data={data} renderItem={ListItemWithIcon} keyExtractor={item => item.id.toString()} />
	);
};

TableListTreasureScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	database: PropTypes.oneOfType([PropTypes.object, PropTypes.oneOf([false])]).isRequired,
};

export { TableListTreasureScreen as default };
