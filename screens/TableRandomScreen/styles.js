import { StyleService } from '@ui-kitten/components';

import { scale, getMarginDirection } from '../../styles';

const styles = StyleService.create({
	mainContainer: {
		flex: 1,
		position: 'relative',
	},
	listContainer: {
		flex: 9,
	},
	backdrop: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
	},
	cardContainer: { minWidth: '80%' },
	circle: {
		...getMarginDirection(10),
		alignSelf: 'center',
		backgroundColor: 'color-danger-600',
		width: scale(30),
		height: scale(30),
		borderRadius: scale(30 / 2),
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export { styles as default };
