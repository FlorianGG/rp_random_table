import PropTypes from 'prop-types';
import React, { useEffect, useState, useCallback, useRef } from 'react';
import { View } from 'react-native';
import {
	List,
	useTheme,
	Layout,
	Modal,
	Card,
	Button,
	Text,
	useStyleSheet,
} from '@ui-kitten/components';

import WaitingComponent from '../../components/WaitingComponent';
import ListItemTableDiceMemo from '../../components/ListItemTableDice';
import HeaderRandom from '../../components/HeaderRandom';
import FuckingBottomButton from '../../components/FuckingBottomButton';

import { fullProptypes } from '../../services/proptypes';
import { FUNCTIONS } from '../../services';

import styles from './styles';

const randomNumber = () => {
	const number = Math.floor(Math.random() * (100 - 1 + 1)) + 1;
	return number.toString();
};

const TableRandomScreen = ({ database, route, parameters }) => {
	const { table } = route.params;
	const standardizedList = useRef(new Map());
	const [randomeDice, setRandomDice] = useState(randomNumber);
	const [isLoading, setIsLoading] = useState(true);
	const [error, setError] = useState(null);
	const [data, setData] = useState([]);
	const [visible, setVisible] = useState(false);
	const theme = useTheme();
	const themedStyles = useStyleSheet(styles);

	console.log('-------parameters-------');
	console.log(parameters);

	useEffect(() => {
		database
			.selectAllLinesFormOneTable(table.id)
			.then(res => {
				for (let i = 0; i < res.length; i++) {
					const test = res[i].roll_dice.match(/^([0-9]{1,2})_([0-9]{1,3})$/);
					if (test) {
						const test1 = Number.parseFloat(test[1]);
						const test2 = Number.parseFloat(test[2]);
						let index = test1;
						if (test1 <= test2) {
							while (index <= test2) {
								standardizedList.current.set(index.toString(), res[i]);
								index++;
							}
						}
					} else {
						standardizedList.current.set(res[i].roll_dice.toString(), res[i]);
					}
				}
				if (standardizedList.current.size !== 100) {
					setError('Erreur sur la longueur');
				} else {
					setData(res);
				}
			})
			.catch(err => {
				console.log('-------err-------');
				console.log(err);
			});
	}, []);

	useEffect(() => {
		if (randomeDice) {
			setVisible(true);
		}
	}, [randomeDice]);

	useEffect(() => {
		if (data.length) {
			setIsLoading(false);
		}
	}, [data]);

	const ListItemWithTheme = useCallback(
		params => <ListItemTableDiceMemo {...params} theme={theme} />,
		[theme]
	);

	const onPressRandomDice = useCallback(() => {
		setRandomDice(randomNumber());
	}, [setRandomDice]);

	const setInvisible = useCallback(() => {
		setVisible(false);
	}, []);

	const renderTextModal = useCallback(() => {
		if (standardizedList.current && standardizedList.current.get(randomeDice)) {
			if (parameters.isFrenchLanguage) {
				return standardizedList.current.get(randomeDice).french_text;
			} else {
				return standardizedList.current.get(randomeDice).english_text;
			}
		} else {
			return FUNCTIONS.returnText('nothing_to_display', parameters.isFrenchLanguage);
		}
	}, [standardizedList, randomeDice]);

	const ButtonFooter = useCallback(
		() => (
			<View>
				<Button size="small" status="danger" onPress={setInvisible}>
					{FUNCTIONS.returnText('close', parameters.isFrenchLanguage)}
				</Button>
			</View>
		),
		[]
	);

	const DiceResult = useCallback(
		() => (
			<Layout style={themedStyles.circle}>
				<Text>{randomeDice}</Text>
			</Layout>
		),
		[randomeDice]
	);

	return isLoading ? (
		<WaitingComponent isLoading={isLoading} error={error} />
	) : (
		<Layout style={styles.mainContainer}>
			<HeaderRandom item={standardizedList.current.get(randomeDice)} table={table} />
			<Layout style={styles.listContainer}>
				<List
					data={data}
					renderItem={ListItemWithTheme}
					keyExtractor={item => item.id.toString()}
					windowSize={17}
				/>
			</Layout>
			<FuckingBottomButton onPress={onPressRandomDice} />
			<Modal
				visible={standardizedList.current && visible}
				backdropStyle={styles.backdrop}
				onBackdropPress={setInvisible}
			>
				<Card disabled style={styles.cardContainer} header={DiceResult} footer={ButtonFooter}>
					<Text>{renderTextModal()}</Text>
				</Card>
			</Modal>
		</Layout>
	);
};

TableRandomScreen.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			table: PropTypes.shape({
				id: PropTypes.number.isRequired,
				french_text: PropTypes.string.isRequired,
				english_text: PropTypes.string.isRequired,
			}).isRequired,
		}).isRequired,
	}).isRequired,
	...fullProptypes,
};

export { TableRandomScreen as default };
