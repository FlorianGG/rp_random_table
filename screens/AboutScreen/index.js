import React from 'react';
import { Layout, Text } from '@ui-kitten/components';

import styles from './styles';

const AboutScreen = () => {
	return (
		<Layout style={styles.container} level="3">
			<Text>
				Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolores laborum porro,
				consequuntur sequi necessitatibus nulla rem placeat quia magnam quisquam accusamus
				laboriosam, dolorem pariatur in tempora culpa! Facilis, odio cumque?
			</Text>
		</Layout>
	);
};

export default AboutScreen;

AboutScreen.propTypes = {};
AboutScreen.defaultProps = {};
