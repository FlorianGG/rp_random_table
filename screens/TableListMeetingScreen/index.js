import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { Icon, List } from '@ui-kitten/components';

import WaitingComponent from '../../components/WaitingComponent';
import ListItem from '../../components/ListItem';

const PeopleIcon = style => <Icon name="people-outline" {...style} />;

const TableListMeetingScreen = ({ navigation, database }) => {
	const [isLoading, setIsLoading] = useState(true);
	const [error, setError] = useState(null);
	const [data, setData] = useState([]);
	useEffect(() => {
		database
			.selectAllMeetingTable()
			.then(res => {
				setData(res);
			})
			.catch(err => {
				console.log('-------err-------');
				console.log(err);
			});
	}, []);

	useEffect(() => {
		if (data.length) {
			setIsLoading(false);
		}
	}, [data]);

	const ListItemWithIcon = useCallback(params => {
		const navigate = () => navigation.navigate('TableRandomMeeting', { table: params.item });
		return <ListItem {...params} icon={PeopleIcon} onPress={navigate} />;
	}, []);

	return isLoading ? (
		<WaitingComponent isLoading={isLoading} error={error} />
	) : (
		<List data={data} renderItem={ListItemWithIcon} keyExtractor={item => item.id.toString()} />
	);
};

TableListMeetingScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
	database: PropTypes.oneOfType([PropTypes.object, PropTypes.oneOf([false])]).isRequired,
};

export { TableListMeetingScreen as default };
