import { useState, useEffect, useRef } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Convert const Infinity to string to avoid the transformation to null
 * @param {*} key
 * @param {*} value
 */
function stringifyInfinity(key, value) {
	return value === Infinity ? 'Infinity' : value;
}

/**
 * Convert string Infinity to the const number Infinity
 * @param {*} key
 * @param {*} value
 */
function parseInfinity(key, value) {
	return value === 'Infinity' ? Infinity : value;
}

/**
 * Fonction de state persistant générique
 * @param {string} key
 * @param {*} defaultValue
 */
const useAsyncStorage = (key, defaultValue) => {
	const [storageValue, setStorageValue] = useState(defaultValue);

	async function getFromStorage() {
		const fromStorage = await AsyncStorage.getItem(key);
		let value = defaultValue;
		if (fromStorage) {
			value = JSON.parse(fromStorage, parseInfinity);
		}
		setStorageValue(value);
	}

	async function updateStorage(newValue) {
		setStorageValue(newValue);
		await AsyncStorage.setItem(key, JSON.stringify(newValue, stringifyInfinity));
	}

	useEffect(() => {
		getFromStorage();
	}, []);
	return [storageValue, updateStorage];
};

/**
 * Fonction qui permet de gérer la version précédente d'un state
 * @param {*} value
 */
function usePrevisous(value) {
	const ref = useRef();
	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
}

export { useAsyncStorage, usePrevisous, stringifyInfinity, parseInfinity };
