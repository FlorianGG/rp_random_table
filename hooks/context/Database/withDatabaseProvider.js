import React, { PureComponent } from 'react';
import Database from '../../../services/database';

export const DatabaseContext = React.createContext({ database: {} });

const withDatabase = Component => {
	class DatabaseProvider extends PureComponent {
		constructor() {
			super();
			this.state = {
				database: {},
			};
		}

		componentDidMount() {
			const db = new Database();
			db.initDatabase()
				.then(database => {
					this.setState({ database });
				})
				.catch(err => {
					console.log('-------err-------');
					console.log(err);
					this.setState({ database: false });
				});
		}

		render() {
			const { database } = this.state;
			return (
				<DatabaseContext.Provider value={database}>
					<Component {...this.props} />
				</DatabaseContext.Provider>
			);
		}
	}
	return DatabaseProvider;
};
export default withDatabase;
