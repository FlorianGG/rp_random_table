import React from 'react';

import { DatabaseContext } from './withDatabaseProvider';

const withDatabaseConsumer = Component => props => (
	<DatabaseContext.Consumer>
		{database => <Component {...props} database={database} />}
	</DatabaseContext.Consumer>
);
export default withDatabaseConsumer;
