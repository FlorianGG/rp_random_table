import React, { createContext } from 'react';

import { FUNCTIONS } from '../../../services';

const ParametersContext = createContext({
	parameters: {
		theme: 'light',
	},
	toggleTheme: () => {},
	toggleLanguage: () => {},
});

const withParametersContextConsumer = Component => props => (
	<ParametersContext.Consumer>
		{parametersContext => (
			<Component {...props} {...parametersContext} translate={FUNCTIONS.returnText} />
		)}
	</ParametersContext.Consumer>
);
export { ParametersContext as default, withParametersContextConsumer };
