import React, { PureComponent } from 'react';

import AsyncStorage from '@react-native-community/async-storage';

import * as RNLocalize from 'react-native-localize';
import Database from '../../../services/database';

export const GlobalContext = React.createContext({});

const withGlobalContextProvider = Component => {
	class globalProvider extends PureComponent {
		constructor() {
			super();
			this.state = {
				database: {},
				theme: 'light',
				isFrenchLanguage: RNLocalize.getCountry() === 'FR',
				isLoadingInit: true,
			};
		}

		componentDidMount() {
			const db = new Database();
			const initDatabaseRequest = db
				.initDatabase()
				.then(database => database)
				.catch(err => {
					console.log('-------err-------');
					console.log(err);
					return false;
				});
			const initLanguageState = AsyncStorage.getItem('languagePersistantState')
				.then(res => res)
				.catch(err => {
					console.log('-------err-------');
					console.log(err);
					return null;
				});
			const initThemeState = AsyncStorage.getItem('themePersistantState')
				.then(res => res)
				.catch(err => {
					console.log('-------err-------');
					console.log(err);
					return 'light';
				});
			return Promise.all([initDatabaseRequest, initLanguageState, initThemeState]).then(
				resPromises => {
					const [database, isFrenchLanguage, theme] = resPromises;
					const newState = {
						isLoadingInit: false,
						database,
					};
					if (isFrenchLanguage != null) {
						newState.isFrenchLanguage = isFrenchLanguage;
					}
					if (theme === 'light' || theme === 'dark') {
						newState.theme = theme;
					}
					this.setState(newState);
				}
			);
		}

		toggleTheme = () => {
			const { theme } = this.state;
			const newTheme = theme === 'light' ? 'dark' : 'light';
			AsyncStorage.setItem('themePersistantState', newTheme).then(() =>
				this.setState({ theme: newTheme })
			);
		};

		toggleLanguage = () => {
			const { isFrenchLanguage } = this.state;
			this.setState({ isFrenchLanguage: !isFrenchLanguage });
		};

		render() {
			const { database, theme, isFrenchLanguage, isLoadingInit } = this.state;
			return (
				<GlobalContext.Provider
					value={{
						database,
						theme,
						isFrenchLanguage,
						isLoadingInit,
						toggleTheme: this.toggleTheme,
						toggleLanguage: this.toggleLanguage,
					}}
				>
					<Component {...this.props} />
				</GlobalContext.Provider>
			);
		}
	}
	return globalProvider;
};
export default withGlobalContextProvider;
