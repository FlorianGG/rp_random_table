import React from 'react';

import { GlobalContext } from './withGlobalContextProvider';

const withGlobalContextConsumer = Component => props => (
	<GlobalContext.Consumer>
		{globalContext => <Component {...props} {...globalContext} />}
	</GlobalContext.Consumer>
);

export default withGlobalContextConsumer;
